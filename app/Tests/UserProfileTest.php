<?php

namespace App\Tests;

use GuzzleHttp\Client;

class UserProfileTest extends  BaseApiTest
{
    /** @test */
    public function getUserProfileSuccess()
    {
        $id = 4; // mark
        $client = new Client();
        $response = $client->request('GET','http://' .$this->host. '/user/'.$id.'/profile');
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(true), true);
        $this->assertArrayHasKey('user',$data);
        $this->assertArrayHasKey('last_name',$data['user']);
        $this->assertArrayHasKey('first_name',$data['user']);
    }
    /** @test */
    public function getUserProfileFailure()
    {
        $id = 8; // no existe
        $client = new Client();
        $response = $client->request('GET','http://' .$this->host. '/user/'.$id.'/profile',['http_errors' => false]);
        $this->assertEquals(400, $response->getStatusCode());
        $data = json_decode($response->getBody(true), true);
        $this->assertArrayHasKey('msg',$data);
    }
}
