<?php
namespace App\Tests;

use Dotenv\Dotenv;

class BaseApiTest extends \PHPUnit_Framework_TestCase
{
    protected $host;
    
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $dotenv = new Dotenv(__DIR__ . '/../..');
        $dotenv->load();
        $this->host = getenv('HOST_DOMAIN');
    }

}
