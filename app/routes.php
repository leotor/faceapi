<?php

$app->get('/', 'ApiDocsController:index');

$app->get('/doc', 'ApiDocsController:index');

$app->get('/doc/json', 'ApiDocsController:getJson');

$app->get('/user/{id}/profile', 'UsersController:getProfile');

