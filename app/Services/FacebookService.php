<?php

namespace App\Services;

use Slim\Http\Response;

class FacebookService
{
    private $fb;

    public function __construct()
    {
        $this->fb = new \Facebook\Facebook([
           'app_id' => getenv('FACE_SDK_APP_ID'),
           'app_secret'=> getenv('FACE_SDK_APP_SECRET'),
           'default_graph_version' => 'v2.9',
           'default_access_token' => getenv('FACE_SDK_APP_ID').'|'.getenv('FACE_SDK_APP_SECRET'),
        ]);
    }

    public function getUserProfile($id, Response $response)
    {
        if (!is_numeric($id))
            return $response->withJson(['msg'=>'Invalid id. Id must be a number'],400);

        try {
            $fbResponse = $this->fb->get('/'.$id.'?fields=id, first_name, last_name, gender, cover, picture, age_range, locale, link, timezone, updated_time, verified');

        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            return $response->withJson(['msg' => $e->getMessage()],400);

        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            return $response->withJson(['msg' => $e->getMessage()],500);

        }
        $user = $fbResponse->getDecodedBody();
        return $response->withJson(['user'=> $user], 200);
    }
}
