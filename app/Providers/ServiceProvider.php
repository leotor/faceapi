<?php
//Register your own services..
const CUSTOM_SERVICES = [
    'service.facebook' => 'App\Services\FacebookService',
    'service.renderer' => [
        'Slim\Views\PhpRenderer',
        __DIR__ . '/../Resources/views/'
    ],
    //..
];


//Add each service to the container..
foreach (CUSTOM_SERVICES as $serviceName => $class) {
    if (is_array($class))
        $container[$serviceName] = new $class[0]($class[1]);
    else
        $container[$serviceName] = new $class();
}
