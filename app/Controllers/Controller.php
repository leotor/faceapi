<?php

namespace App\Controllers;


Class Controller
{
    protected $container;
    const ROOT_DIR = __DIR__ . '/../..';

    public function __construct($container)
    {
        $this->container = $container;
    }

    //Access to the container through get method
    protected function get($property)
    {
        if ($this->container->{$property}) {
            return $this->container->{$property};
        }
    }
}
