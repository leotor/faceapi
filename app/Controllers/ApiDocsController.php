<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class ApiDocsController extends Controller
{
    public function index($request,$response)
    {
        $this->get('service.renderer')->render($response,"apidocs/index.html");
    }

    public function getJson(Request $request, Response $response)
    {
        try {
            $swagger = \Swagger\scan(__DIR__);
            return $response->withJson($swagger,200);
        } catch (\Exception $e) {
            return $response->withJson($swagger,500);
        }
    }
}
