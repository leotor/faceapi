<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class UsersController extends BaseApiController
{
    /**
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     *
     * @return mixed
     * @SWG\Get(
     *     path="/user/{id}/profile",
     *     description="Returns  user's  public facebook profile. <br> it uses app token.",
     *     produces={"application/json"},
     *     tags={"profile"},
     *     @SWG\Parameter(
     *          name="id",
     *          description="The ID of the facebook user",
     *          type="integer",
     *          format="int32",
     *          in="path",
     *          required=true
     *     ),
     *     @SWG\Response(response=200, description="User Information facebook profile."),
     *     @SWG\Response(response=401, description="Unauthorized action."),
     *     @SWG\Response(response=500, description="Internal error server.",
     *     )
     * )
     */
    public function getProfile(Request $request, Response $response)
    {
        $content = $this->get('service.facebook')->getUserProfile($request->getAttribute('id'),$response);
        return $content;
    }
}
