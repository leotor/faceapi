<?php

namespace App\Controllers;

/**
 *
 * @SWG\Swagger(
 *     basePath="",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="0.1",
 *         title="Aivo Test",
 *         @SWG\Contact(name="Torcello, Leandro",email="leandro.torcello@gmail.com"),
 *     ),
 * )
 */
Class BaseApiController extends Controller
{

}
