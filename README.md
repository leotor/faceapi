#### INSTALLATION

```
$composer install
$cp .env.example .env
```
You should modify the HOST_DOMAIN with ur local domain. This is necessary to run the tests


#### RUN
For apache/nginx web servers, point the virtual host to public/ folder.
For built-in php server, run ```$php -S localhost:8000 -t public/```

#### TEST

```
$vendor/phpunit/phpunit/phpunit app/Tests/UserProfileTest.php
```

#### API DOCUMENTATION
Open http://<your-url>/doc or http://<your-url> , with your favorite browser.
