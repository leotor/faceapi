<?php

require __DIR__. '/../vendor/autoload.php';

//Register .env..
$dotenv = new Dotenv\Dotenv(__DIR__ . '/..');
$dotenv->load();

//Start Slim..
$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => (bool)getenv('DISPLAY_ERRORS'),
    ]
]);
//Declarations..
$container = $app->getContainer();

//Service Registration file..
require __DIR__ . '/../app/Providers/ServiceProvider.php';

//Controller registration
$container['UsersController'] = function($container) {
    return new \App\Controllers\UsersController($container);
};
$container['ApiDocsController'] = function($container) {
    return new \App\Controllers\ApiDocsController($container);
};

//Routes file
require __DIR__ . '/../app/routes.php';
